package com.example.springrestlawencon.helpers;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class CommonHelper {
     public static long countDates(String start, String end) {
        LocalDate startDate = LocalDate.parse(start);
        LocalDate endDate = LocalDate.parse(end);

        return countDatesInRange(startDate, endDate);
    }
    private static long countDatesInRange(LocalDate startDate, LocalDate endDate) {
        // Plus 1 to include both start and end dates in the count
        return ChronoUnit.DAYS.between(startDate, endDate) + 1;
    }
}
