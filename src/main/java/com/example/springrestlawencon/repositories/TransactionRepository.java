package com.example.springrestlawencon.repositories;

import com.example.springrestlawencon.model.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    @Query("SELECT u FROM Transaction u WHERE u.pwKembalikan = :pw")
    Transaction findUserByPassword(@Param("pw") String password);
}
