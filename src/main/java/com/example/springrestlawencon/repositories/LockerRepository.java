package com.example.springrestlawencon.repositories;
import org.springframework.data.jpa.repository.JpaRepository;

import com.example.springrestlawencon.model.Locker;

public interface LockerRepository extends JpaRepository<Locker, Long> {
    Locker findFirstById(Long id);
}
