package com.example.springrestlawencon.repositories;

import com.example.springrestlawencon.model.EmailDetails;
import com.example.springrestlawencon.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {

    @Query("SELECT u FROM User u WHERE u.email = :email")
    User findUserByEmail(@Param("email") String email);

    interface EmailService {

        // Method
        // To send a simple email
        String sendSimpleMail(EmailDetails details);

        // Method
        // To send an email with attachment
        String sendMailWithAttachment(EmailDetails details);
    }
}
