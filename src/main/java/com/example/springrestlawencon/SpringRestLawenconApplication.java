package com.example.springrestlawencon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringRestLawenconApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringRestLawenconApplication.class, args);
	}

}
