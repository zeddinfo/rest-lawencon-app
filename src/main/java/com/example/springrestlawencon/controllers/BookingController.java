package com.example.springrestlawencon.controllers;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import com.example.springrestlawencon.model.Transaction;
import com.example.springrestlawencon.model.request.OpenLockerReq;
import com.example.springrestlawencon.repositories.LockerRepository;
import com.example.springrestlawencon.repositories.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.springrestlawencon.helpers.CommonHelper;
import com.example.springrestlawencon.model.EmailDetails;
import com.example.springrestlawencon.model.Locker;
import com.example.springrestlawencon.model.User;
import com.example.springrestlawencon.model.request.BookReq;
import com.example.springrestlawencon.model.response.ErrorRes;
import com.example.springrestlawencon.repositories.UserRepository;

import java.util.Date;
import java.util.Random;

@Controller
@RequestMapping("/rest/book")
public class BookingController {
    @Autowired
    UserRepository userRepository;
    @Autowired
    UserRepository.EmailService emailService;
    @Autowired
    LockerRepository lockerRepository;
    @Autowired
    TransactionRepository transactionRepository;


    @ResponseBody
    @RequestMapping(value = "/locker",method = RequestMethod.POST)
    public ResponseEntity login(@RequestBody BookReq req)  {

        try {
           Object email = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
           User user = userRepository.findUserByEmail(email.toString());
           if(user == null) {
            throw new Exception("user email "+email+" tidak ditemukan");
           }

           if(req.getDeposit().compareTo(new BigDecimal("10000")) != 0) {
            throw new Exception("minimal deposit adalah 10k");
           }

           long jmlHari = CommonHelper.countDates(req.getStartDate(), req.getEndDate());
           long denda = 5000 * jmlHari;

           if(req.getLockers().isEmpty()) {
            throw new Exception("Loker harus dipilih");
           }

           if(req.getLockers().size() > 3) {
            throw new Exception("Jumlah maksimal loker untuk satu kali peminjaman adalah 3");
           }


           for(Locker r : req.getLockers()) {
                Locker locker = lockerRepository.findFirstById(r.getId());
                if(!locker.isAvailable()) {
                    throw new Exception("Mohon maaf locker id " + r.getId() + " tidak ditemukan");
                }

                String pwOpen = generateRandomNumberString();
                String pwBack = generateRandomNumberString();

               Transaction transaction = new Transaction();
               transaction.setDeposit(BigDecimal.valueOf(jmlHari > 1 ? ((5000 * jmlHari) + 10000) : 10000));
               transaction.setPenalty(BigDecimal.valueOf(jmlHari > 1 ? (5000 * jmlHari) : 5000));
               transaction.setLocker(r);
               transaction.setStartDate(convertStringToDate(req.getStartDate()));
               transaction.setEndDate(convertStringToDate(req.getEndDate()));
               transaction.setReturned(false);
               transaction.setPwSimpan(pwOpen);
               transaction.setPwKembalikan(pwBack);
               transactionRepository.save(transaction);

               EmailDetails mail = new EmailDetails();
               mail.setRecipient(email.toString());
               mail.setSubject("Transaksi Booking Loker");
               mail.setMsgBody("Hallo, berikut password untuk membuka loker : "+pwOpen+" , dan berikut untuk mengembalikan loker : "+pwBack);

               emailService.sendSimpleMail(mail);
           }


        }catch (BadCredentialsException e){
            ErrorRes errorResponse = new ErrorRes(HttpStatus.BAD_REQUEST,"Invalid username or password");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
        }catch (Exception e){
            ErrorRes errorResponse = new ErrorRes(HttpStatus.BAD_REQUEST, e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
        }


        return ResponseEntity.ok().build();
    }

    @ResponseBody
    @RequestMapping(value = "/open-locker",method = RequestMethod.POST)
    public ResponseEntity openLocker(@RequestBody OpenLockerReq req) {
        try {
            Transaction transaction = transactionRepository.findUserByPassword(req.getKey());
            if(transaction == null) {
                throw new Exception("Transaksi tidak ditemukan");
            }

            transaction.setReturned(true);
            transactionRepository.save(transaction);
        } catch (Exception e) {

        }

        return ResponseEntity.ok().build();
    }

    public static Date convertStringToDate(String dateString) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return dateFormat.parse(dateString);
    }

    public static String generateRandomNumberString() {
        int length = 6;
        String characters = "0123456789";

        StringBuilder stringBuilder = new StringBuilder();

        Random random = new Random();
        for (int i = 0; i < length; i++) {
            int index = random.nextInt(characters.length());
            stringBuilder.append(characters.charAt(index));
        }


        return stringBuilder.toString();
    }

}
