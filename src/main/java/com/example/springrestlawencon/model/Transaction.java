package com.example.springrestlawencon.model;

import java.math.BigDecimal;
import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "Transaction") // You can specify the table name if it differs from the entity name
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "locker_id", referencedColumnName = "id")
    private Locker locker;

    @Column(name = "start_date", nullable = false)
    private Date startDate;

    @Column(name = "end_date")
    private Date endDate;

    @Column(name = "deposit", precision = 10, scale = 2, nullable = false)
    private BigDecimal deposit;

    @Column(name = "penalty", precision = 10, scale = 2, nullable = false)
    private BigDecimal penalty;

    @Column(name = "is_returned", nullable = false)
    private boolean returned;

    @Column(name = "pw_simpan")
    private String pwSimpan;
    @Column(name = "pw_kembalikan")
    private String pwKembalikan;

    // Constructors, getters, setters
}
