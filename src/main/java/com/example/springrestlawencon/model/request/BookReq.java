package com.example.springrestlawencon.model.request;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import com.example.springrestlawencon.model.Locker;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BookReq implements Serializable{
    private String noHp;
    private String startDate;
    private String endDate;
    private BigDecimal deposit;
    private BigDecimal penalty;
    private List<Locker> lockers;
}
