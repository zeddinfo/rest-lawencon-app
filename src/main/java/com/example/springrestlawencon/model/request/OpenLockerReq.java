package com.example.springrestlawencon.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OpenLockerReq implements Serializable {
    private String key;
    private Integer id;
}
