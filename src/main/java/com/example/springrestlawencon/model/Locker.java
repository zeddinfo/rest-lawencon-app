package com.example.springrestlawencon.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "locker") // You can specify the table name if it differs from the entity name
public class Locker {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "locker_number", nullable = false)
    private int lockerNumber;

    @Column(name = "password", nullable = false, length = 50)
    private String password;

    @Column(name = "is_available", nullable = false)
    private boolean available;

    // Constructors, getters, setters
}
